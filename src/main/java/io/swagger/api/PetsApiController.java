package io.swagger.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

import io.swagger.annotations.ApiParam;
import io.swagger.model.NewPet;
import io.swagger.model.Pet;
import io.swagger.repository.PetRepository;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-02-22T14:56:36.277Z")

@Controller
public class PetsApiController implements PetsApi
{

	@Autowired
	private PetRepository petRepository;

	public ResponseEntity<Pet> addPet(
			@ApiParam(value = "Pet to add to the store", required = true) @RequestBody NewPet pet)
	{
		// do some magic!
		return new ResponseEntity<Pet>(HttpStatus.OK);
	}

	public ResponseEntity<Void> deletePet(
			@ApiParam(value = "ID of pet to delete", required = true) @PathVariable("id") Long id)
	{
		// do some magic!
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	public ResponseEntity<Pet> findPetById(
			@ApiParam(value = "ID of pet to fetch", required = true) @PathVariable("id") Long id)
	{
		// do some magic!
		// Pet pet=new Pet();
		// pet.setName("monkey");
		// pet.setId(1L);
		// pet.setTag("dog");

		return new ResponseEntity<Pet>(petRepository.findOne(1L),
				HttpStatus.OK);
	}

	public ResponseEntity<List<Pet>> findPets(
			@ApiParam(value = "tags to filter by") @RequestParam(value = "tags", required = false) List<String> tags,
			@ApiParam(value = "maximum number of results to return") @RequestParam(value = "limit", required = false) Integer limit)
	{
		// do some magic!
		return new ResponseEntity<List<Pet>>(HttpStatus.OK);
	}

}
