package io.swagger.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import io.swagger.model.Pet;

/**
 * Created by JackPeng(pengjunkun@gmail.com) on 22/02/2017.
 */
@Repository
public interface PetRepository extends JpaRepository<Pet, Long>
{
}
